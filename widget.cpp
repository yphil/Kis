/*
  LV2 Sinewave synth plugin : based on eg-amp
*/

#include "lv2/lv2plug.in/ns/extensions/ui/ui.h"
#include <iostream>

#include "widget.h"
#include "images.h"

#include <FL/Fl.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Dial.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Box.H>
#include <FL/x.H>

using namespace std;

static void VolumeCallback(Fl_Widget *w, void* userdata) {

  Widget* self = (Widget*)userdata;
  Fl_Dial* this_widget = (Fl_Dial*)w;

  // float newFrequency = 220 + 220 * (1-sliderWidget->value());

  float newVolume = (this_widget->value());

  // which port to write to, check the .ttl file for the index
  int portNumber = 3;

  printf("Yo : %f\n", newVolume);

  // here we use the LV2UI_Controller and LV2UI_write_function "things"
  // to write some data to a port
  self->write_function( self->controller, portNumber, sizeof(float), 0, (const void*)&newVolume);
}


static void CutoffCallback(Fl_Widget *w, void* userdata) {

  Widget* self = (Widget*)userdata;
  Fl_Slider* sliderWidget = (Fl_Slider*)w;
  float newCutoff = (sliderWidget->value());
  int portNumber = 4;

  self->write_function( self->controller, portNumber, sizeof(float), 0, (const void*)&newCutoff);
}


static void ResonanceCallback(Fl_Widget *w, void* userdata) {

  Widget* self = (Widget*)userdata;
  Fl_Slider* sliderWidget = (Fl_Slider*)w;
  float newResonance = (sliderWidget->value());
  int portNumber = 5;

  self->write_function( self->controller, portNumber, sizeof(float), 0, (const void*)&newResonance);
}

static void EnvelopeCallback(Fl_Widget *w, void* userdata) {

  Widget* self = (Widget*)userdata;
  Fl_Slider* sliderWidget = (Fl_Slider*)w;
  float newEnvelope = (sliderWidget->value());
  int portNumber = 6;

  self->write_function( self->controller, portNumber, sizeof(float), 0, (const void*)&newEnvelope);
}

static void PortamentoCallback(Fl_Widget *w, void* userdata) {

  Widget* self = (Widget*)userdata;
  Fl_Slider* sliderWidget = (Fl_Slider*)w;
  float newPortamento = (sliderWidget->value());
  int portNumber = 7;

  self->write_function( self->controller, portNumber, sizeof(float), 0, (const void*)&newPortamento);
}

static void ReleaseCallback(Fl_Widget *w, void* userdata) {

  Widget* self = (Widget*)userdata;
  Fl_Slider* sliderWidget = (Fl_Slider*)w;
  float newRelease = (sliderWidget->value());
  int portNumber = 8;
  self->write_function( self->controller, portNumber, sizeof(float), 0, (const void*)&newRelease);
}

// void Widget::cb_volumeButton(Fl_Widget* o, void*) {
//   float tmp = o->value();
//   VolumeButton->value( tmp );
// }

Widget::Widget(void* parentXwindow)
{


  // In case FLTK hasn't set up yet
  fl_open_display();

  win = new Fl_Double_Window(400, 135);

  // Fl_Text_Display* Text_Display = new Fl_Text_Display( 33,5,33,155,"Kis");
  Fl_Box* Box = new Fl_Box(260, 15, 135, 117,"");

  Box->image(image_kis);

  VolumeButton = new Fl_Dial( 5,60,50,45,"Vol");
  CutoffButton = new Fl_Slider( 60,5,35,100,"Cut");
  ResonanceButton = new Fl_Slider( 100,5,35,100,"Res");
  EnvelopeButton = new Fl_Slider( 140,5,35,100,"Env");
  PortamentoButton = new Fl_Slider( 180,5,35,100,"Por");
  ReleaseButton = new Fl_Slider( 220,5,35,100,"Rel");

  VolumeButton->tooltip("Volume");
  VolumeButton->minimum(0);
  VolumeButton->maximum(127);
  VolumeButton->step(1);
  VolumeButton->when(FL_WHEN_CHANGED);

  CutoffButton->tooltip("Cutoff");
  CutoffButton->minimum(127);
  CutoffButton->maximum(0);
  CutoffButton->step(1);

  ResonanceButton->tooltip("Resonance");
  ResonanceButton->minimum(0);
  ResonanceButton->maximum(127);
  ResonanceButton->step(1);

  EnvelopeButton->tooltip("Envelope");
  EnvelopeButton->minimum(0);
  EnvelopeButton->maximum(127);
  EnvelopeButton->step(1);

  PortamentoButton->tooltip("Portamento");
  PortamentoButton->minimum(0);
  PortamentoButton->maximum(127);
  PortamentoButton->step(1);

  ReleaseButton->tooltip("Portamento Release time");
  ReleaseButton->minimum(0);
  ReleaseButton->maximum(127);
  ReleaseButton->step(1);

  VolumeButton->callback( VolumeCallback, this );
  CutoffButton->callback( CutoffCallback, this );
  ResonanceButton->callback( ResonanceCallback, this );
  EnvelopeButton->callback( EnvelopeCallback, this );
  PortamentoButton->callback( PortamentoCallback, this );
  ReleaseButton->callback( ReleaseCallback, this );

  win->end();

  win->resizable( 0 );

  win->border(0);

  fl_embed( win, (Window)parentXwindow );

}

int Widget::show()
{
  win->show();
  return 0;
}

int Widget::hide()
{
  win->hide();
  return 0;
}

int Widget::idle()
{
  // takes care of handling events
  Fl::check();
  Fl::flush();

  // if window is closed during handling of events, notify the host that we've
  // closed the window.
  if ( !win->shown() )
    return 1;

  return 0;
};
