/*
  LV2 Sinewave synth plugin : based on so_404
*/

#ifndef WIDGET
#define WIDGET

#include <FL/Fl.H>
#include <FL/Fl_Single_Window.H>

#include "lv2/lv2plug.in/ns/extensions/ui/ui.h"
using namespace std;
#include <stdio.h>
#include <iostream>
#include <FL/Fl_Box.H>
#include <FL/Fl_Dial.H>
#include <cmath>
#include <FL/Fl_Slider.H>

// LV2UI stuff
extern "C" {
#include "lv2/lv2plug.in/ns/extensions/ui/ui.h"
}

class Widget
{

 public:
  Fl_Dial* VolumeButton;
  Fl_Slider* CutoffButton;
  Fl_Slider* ResonanceButton;
  Fl_Slider* EnvelopeButton;
  Fl_Slider* PortamentoButton;
  Fl_Slider* ReleaseButton;

  // public Lv2 communication stuff, gets set in instantiate
  LV2UI_Controller controller;
  LV2UI_Write_Function write_function;

  // xWindowID to embed into
  Widget(void* xWindowID);

  // gets called repeatedly to update the UI
  int idle();
  int show();
  int hide();

  // int port_event();

  int value(int value) {
    cout << "Called with value " << value << endl;
}


  int getWidth() {return win->w();}
  int getHeight(){return win->h();}

 protected:
  Fl_Window* win;

  // stores values we're currently at.
  float volume;
  float cutoff;
  float resonance;
  float envelope;
  float portamento;
  float release;

};

#endif
