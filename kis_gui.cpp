/*
  LV2 Kis plugin : based on so-404
*/


#include <string>
#include <iostream>

// include the URI and global data of this plugin
#include "kis.h"


// core spec include

// GUI
#include "lv2/lv2plug.in/ns/lv2core/lv2.h"
#include "lv2/lv2plug.in/ns/extensions/ui/ui.h"

#include <FL/x.H>

// this is our custom widget include
#include "widget.h"

using namespace std;

typedef struct {
  Widget* widget;
  LV2UI_Write_Function write_function;
  LV2UI_Controller controller;
} KisGUI;


static LV2UI_Handle instantiate(const struct _LV2UI_Descriptor * descriptor,
                                const char * plugin_uri,
                                const char * bundle_path,
                                LV2UI_Write_Function write_function,
                                LV2UI_Controller controller,
                                LV2UI_Widget * widget,
                                const LV2_Feature * const * features)
{
  if (strcmp(plugin_uri, KIS_URI) != 0) {
    fprintf(stderr, "KIS_URI error: this GUI does not support plugin with URI %s\n", plugin_uri);
    return NULL;
  }

  void* parentXwindow = 0;

  LV2UI_Resize* resize = NULL;

  KisGUI* self = (KisGUI*)malloc(sizeof(KisGUI));
  if (self == NULL) return NULL;

  for (int i = 0; features[i]; ++i) {
    if (!strcmp(features[i]->URI, LV2_UI__parent)) {
      parentXwindow = features[i]->data;
      cout << "got parent UI feature: X11 id = " << (Window)parentXwindow << endl;
    } else if (!strcmp(features[i]->URI, LV2_UI__resize)) {
      resize = (LV2UI_Resize*)features[i]->data;
    }
  }

  self->controller = controller;
  self->write_function = write_function;

  fl_open_display();

  self->widget = new Widget( parentXwindow );

  self->widget->controller = controller;
  self->widget->write_function = write_function;

  if (resize) {
    resize->ui_resize(resize->handle, self->widget->getWidth(),self->widget->getHeight());
  }

  // fl_embed( self->widget->window, (Window)parentXwindow );

  //cout << "returning..." << int(self->widget->getXID()) << endl;

  return (LV2UI_Handle)self;
}

static void cleanup(LV2UI_Handle ui) {
  KisGUI *self = (KisGUI *) ui;
  delete self->widget;
  free(self);
}

static void port_event(LV2UI_Handle ui,
                       uint32_t port_index,
                       uint32_t buffer_size,
                       uint32_t format,
                       const void * buffer)
{
  KisGUI *self = (KisGUI *) ui;

  if ( format == 0 )
    {
      float value = *(float *)buffer;
      // cout << "Port event! Index " << port_index << " Value is " << value << endl;
      self->widget->value(value);
      switch (port_index)
        {
        case 3:
            {self->widget->VolumeButton->value(value);}
          break;
        case 4:
            {self->widget->CutoffButton->value(value);}
          break;
        case 5:
            {self->widget->ResonanceButton->value(value);}
          break;
        case 6:
            {self->widget->EnvelopeButton->value(value);}
          break;
        case 7:
            {self->widget->PortamentoButton->value(value);}
          break;
        case 8:
            {self->widget->ReleaseButton->value(value);}
          break;
        }
    }

  return;
}

static int ui_show(LV2UI_Handle handle)
{
  KisGUI* self = (KisGUI*)handle;
  self->widget->show();
  return 0;
}

static int ui_hide(LV2UI_Handle handle)
{
  KisGUI* self = (KisGUI*)handle;
  return self->widget->hide();
}


static int idle(LV2UI_Handle handle)
{
  KisGUI* self = (KisGUI*)handle;
  self->widget->idle();
  return 0;
}


static const void*
extension_data(const char* uri)
{
  static const LV2UI_Show_Interface show_iface = { ui_show, ui_hide };
  static const LV2UI_Idle_Interface idle_iface = { idle };

  if (!strcmp(uri, LV2_UI__showInterface))
    {
      return (void*)&show_iface;
    }
  else if (!strcmp(uri, LV2_UI__idleInterface))
    {
      return &idle_iface;
    }

  return NULL;
}

static LV2UI_Descriptor descriptors[] = {
  {KIS_UI_URI, instantiate, cleanup, port_event, extension_data}
};

const LV2UI_Descriptor * lv2ui_descriptor(uint32_t index) {
  printf("lv2ui_descriptor(%u) called\n", (unsigned int)index);
  if (index >= sizeof(descriptors) / sizeof(descriptors[0])) {
    return NULL;
  }
  return descriptors + index;
}
