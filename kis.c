/*
  LV2 Kis plugin : based on so-404

*/

#include "kis.h"
// #include "ADSR.h"

#define SAMPLE_RATE 44100

static LV2_Descriptor kis_Descriptor= {
	.URI="https://bitbucket.org/xaccrocheur/kis",
	.instantiate=instantiateKIS,
	.connect_port=connectPortKIS,
	.activate=NULL,
	.run=runKIS,
	.deactivate=NULL,
	.cleanup=cleanupKIS,
	.extension_data=NULL,
};


LV2_SYMBOL_EXPORT
const LV2_Descriptor* lv2_descriptor(uint32_t index) {
  switch(index) {
  case 0:
    return &kis_Descriptor;
  default:
    return NULL;
  }
}


float midi_to_hz(int note) {
  return 440.0*powf( 2.0, (note-69) / 12.0 );
}

void compute(int count, float* array, int freq, int harmonics)
{
  for(float i = 0.f; i < count; i++ )
  {
    // calculate wavelength (in samples) of frequency
    float wavelength = SAMPLE_RATE / freq;

    // temporary value, for adding harmonics before writing to array
    float out = 0.f;

    // loop over each harmonic
    for(int h = 1; h < harmonics + 1; h++ )
    {
      float harmonicFreq = (2*h-1) * freq;

      // check harmonic for aliasing ( > nyquist)
      if ( harmonicFreq < SAMPLE_RATE / 2.f )
      {
        // formula for square wave:
        //    odd harmonics only
        //    harmonic amplitude  = 1 /
        out += sin( (2*h-1) * (i/wavelength) ) / (2*h-1);
      }
    }

    // write the output value of the added harmonics to the array
    array[(int)i] = out / 2.f;
  }
}

void runKIS( LV2_Handle arg, uint32_t nframes ) {
  kis* Kis=(kis*)arg;
  lv2_event_begin(&Kis->in_iterator,Kis->MidiIn);

  float* outbuffer=Kis->output;

  if(*Kis->controlmode_p >0) {
    Kis->cutoff=*Kis->cutoff_p;
    Kis->portamento=*Kis->portamento_p;
    Kis->release=*Kis->release_p;
    Kis->volume=*Kis->volume_p;
    Kis->envmod=*Kis->envmod_p;
    Kis->resonance=*Kis->resonance_p;
  }

  for(int i=0; i<nframes; i++ ) {

    while(lv2_event_is_valid(&Kis->in_iterator)) {
      uint8_t* data;
      LV2_Event* event= lv2_event_get(&Kis->in_iterator,&data);

      if (event->type == 0) {
        Kis->event_ref->lv2_event_unref(Kis->event_ref->callback_data, event);
      } else if(event->type==Kis->midi_event_id) {
        if(event->frames > i) {
          break;

        } else {
          // printf("Note! Frame %f \n", event->type);
          const uint8_t* evt=(uint8_t*)data;
          if((evt[0]&MIDI_CHANNELMASK)==(int) (*Kis->channel_p)) {

            if((evt[0]&MIDI_COMMANDMASK)==MIDI_NOTEON) 	{
              unsigned int note = evt[1];
              Kis->tfreq=midi_to_hz(note);

              if( Kis->noteson == 0 )
                {
                  Kis->freq = Kis->tfreq;
                  Kis->amp=1.0;
                  Kis->vel = ((float)evt[2]);
                  Kis->env=Kis->vel/127.0;
                  Kis->cdelay = 0;
                }
              Kis->noteson += 1;

            } else if((evt[0]&MIDI_COMMANDMASK)==MIDI_NOTEOFF )	{
              Kis->noteson -= 1;
              if(Kis->noteson<0) {
                Kis->noteson=0;
              }

            } else if((*Kis->controlmode_p<=0) && (evt[0]&MIDI_COMMANDMASK)==MIDI_CONTROL )	{
              unsigned int command_val=evt[2];
              switch(evt[1]) {
              case 74:
                Kis->cutoff =command_val;
                break;
              case 65:
                Kis->portamento = command_val;
                break;
              case 72:
                Kis->release = command_val;
                break;
              case 7:
                Kis->volume = command_val;
                break;
              case 79:
                Kis->envmod = command_val;
                break;
              case 71:
                Kis->resonance = command_val;
                break;
              }
            }
          }
        }
      }
      lv2_event_increment(&Kis->in_iterator);

    }
    if( Kis->cdelay <= 0 )
      {
        Kis->freq = ((Kis->portamento/127.0)*0.9)*Kis->freq + (1.0-((Kis->portamento/127.0)*0.9))*Kis->tfreq;
        if( Kis->noteson > 0 ) {
          Kis->amp *= 0.99;
        } else {
          Kis->amp *= 0.5;
        }
        Kis->env*=0.8+powf(Kis->release/127.0,0.25)/5.1;

        Kis->fcutoff = powf(Kis->cutoff/127.0,2.0)+powf(Kis->env,2.0)*powf(Kis->envmod/127.0,2.0);
        Kis->fcutoff = tanh(Kis->fcutoff);
        Kis->freso = powf(Kis->resonance/130.0,0.25);
        Kis->cdelay = Kis->samplerate/100;
      }
    Kis->cdelay--;

    float max = Kis->samplerate / Kis->freq;
    float sample = (Kis->phase/max)*(Kis->phase/max)-0.25;
    Kis->phase++;
    if( Kis->phase >= max ) {
      Kis->phase -= max;
    }

    if(Kis->vel>100) {
      sample*=Kis->env;
    } else {
      sample*=Kis->amp;
    }

    Kis->fpos += Kis->fspeed;
    Kis->fspeed *= Kis->freso;
    Kis->fspeed += (sample-Kis->fpos)*Kis->fcutoff;
    sample = Kis->fpos;

    sample = sample*0.5+Kis->lastsample*0.5;
    Kis->lastsample = sample;

    float array[SAMPLE_RATE];

    // compute( SAMPLE_RATE, &array[0], 220, 50 );

    outbuffer[i] = sample * (Kis->volume/127.0);
  }
}


LV2_Handle
instantiateKIS(const LV2_Descriptor* descriptor,
                  double s_rate,
                  const char *path,
                  const LV2_Feature * const* features)
{
  kis* Kis=malloc(sizeof(kis));

  LV2_URI_Map_Feature *map_feature;
  const LV2_Feature * const *  ft;

  for (ft = features; *ft; ft++) {
    if (!strcmp((*ft)->URI, "http://lv2plug.in/ns/ext/uri-map")) {
      map_feature = (*ft)->data;
      Kis->midi_event_id = map_feature->uri_to_id(
                                                 map_feature->callback_data,
                                                 "http://lv2plug.in/ns/ext/event",
                                                 "http://lv2plug.in/ns/ext/midi#MidiEvent");
    } else if (!strcmp((*ft)->URI, "http://lv2plug.in/ns/ext/event")) {
      Kis->event_ref = (*ft)->data;
    }
  }

  puts("\n");
  puts("██╗  ██╗██╗███████╗");
  puts("██║ ██╔╝██║██╔════╝");
  puts("█████╔╝ ██║███████╗");
  puts("██╔═██╗ ██║╚════██║");
  puts("██║  ██╗██║███████║");
  puts("╚═╝  ╚═╝╚═╝╚══════╝");

  Kis->phase = 0.0;
  Kis->freq = 440.0;
  Kis->tfreq = 440.0;
  Kis->amp = 0.0;
  Kis->env=0.0;
  Kis->vel=0;
  Kis->fcutoff = 0.0;
  Kis->fspeed = 0.0;
  Kis->fpos = 0.0;
  Kis->lastsample = 0.0;
  Kis->noteson = 0;
  Kis->cdelay = s_rate/100;
  Kis->samplerate=s_rate;

  Kis->volume = 100;
  Kis->cutoff = 50;
  Kis->resonance = 100;
  Kis->envmod = 80;
  Kis->portamento = 64;
  Kis->release = 100;

  return Kis;
}


void cleanupKIS(LV2_Handle instance) {
  free(instance);
}


void connectPortKIS(LV2_Handle instance, uint32_t port, void *data_location) {
  kis* Kis=(kis*) instance;
  switch(port) {
  case PORT_KIS_OUTPUT:
    Kis->output=data_location;
    break;
  case PORT_KIS_MIDI:
    Kis->MidiIn=data_location;
    break;
  case PORT_KIS_CONTROLMODE:
    Kis->controlmode_p=data_location;
    break;
  case PORT_KIS_VOLUME:
    Kis->volume_p=data_location;
    break;
  case PORT_KIS_CUTOFF:
    Kis->cutoff_p=data_location;
    break;
  case PORT_KIS_RESONANCE:
    Kis->resonance_p=data_location;
    break;
  case PORT_KIS_ENVELOPE:
    Kis->envmod_p=data_location;
    break;
  case PORT_KIS_PORTAMENTO:
    Kis->portamento_p=data_location;
    break;
  case PORT_KIS_RELEASE:
    Kis->release_p=data_location;
    break;
  case PORT_KIS_CHANNEL:
    Kis->channel_p=data_location;
    break;
  default:
    fputs("Warning, unconnected port!\n",stderr);
  }
}
