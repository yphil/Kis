/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * kis.h - based on SO-404
 *
 * Copyright (C) 2011 - Jeremy Salwen
 * Copyright (C) 2010 - 50m30n3
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <lv2.h>
#include <string.h>
#include "lv2/lv2plug.in/ns/ext/event/event-helpers.h"
#include "lv2/lv2plug.in/ns/ext/uri-map/uri-map.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

#define KIS_URI "https://bitbucket.org/xaccrocheur/kis"
#define KIS_UI_URI "https://bitbucket.org/xaccrocheur/kis#gui"

#define NUMNOTES 80
#define BASENOTE 21

#define MIDI_COMMANDMASK 0xF0
#define MIDI_CHANNELMASK 0x0F

#define MIDI_NOTEON 0x90
#define MIDI_NOTEOFF 0x80
#define MIDI_CONTROL 0xB0

enum PORTS_KIS {
    PORT_KIS_FREQ=0,
	PORT_KIS_OUTPUT=0,
	PORT_KIS_MIDI,
	PORT_KIS_CONTROLMODE,
	PORT_KIS_VOLUME,
	PORT_KIS_CUTOFF,
	PORT_KIS_RESONANCE,
	PORT_KIS_ENVELOPE,
	PORT_KIS_PORTAMENTO,
	PORT_KIS_RELEASE,
	PORT_KIS_CHANNEL
};

void runKIS( LV2_Handle arg, uint32_t nframes );
LV2_Handle instantiateKIS(const LV2_Descriptor *descriptor,double s_rate, const char *path,const LV2_Feature * const* features);
void cleanupKIS(LV2_Handle instance);
void connectPortKIS(LV2_Handle instance, uint32_t port, void *data_location);

typedef struct kis_t {
	float* output;
	LV2_Event_Buffer *MidiIn;
	LV2_Event_Iterator in_iterator;

	LV2_Event_Feature* event_ref;
	int midi_event_id;

	float* controlmode_p;
	float* cutoff_p;
	float* portamento_p;
	float* release_p;
	float* volume_p;
	float* envmod_p;
	float* resonance_p;
	float* channel_p;

    float* freqy;

	float freq, tfreq;

	double samplerate;

	unsigned int cdelay;

	unsigned int cutoff;
	unsigned int resonance;
	unsigned int volume;
	unsigned int portamento;
	unsigned int release;
	unsigned int envmod;
	unsigned int vel;

	float phase;
	float amp;
	float lastsample;
	float env;

	float fcutoff;
	float fspeed;
	float fpos;
	float freso;

	int noteson;

} kis;


#ifdef __cplusplus
}
#endif
